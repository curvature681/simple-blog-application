import { FastifyReply, FastifyRequest, FastifySchema } from 'fastify';
import { CommentType, RequestIdParamType, Comment } from './comment.types';
import { CommentService } from '@/service/comment.service';

const commentService = new CommentService();

export default class GetCommentByPostAction {
  static readonly PATH = '/post/:id/comment';

  public static action = async (
    request: FastifyRequest<{ Params: RequestIdParamType; Reply: CommentType }>,
    reply: FastifyReply
  ) => {
    const { id } = request.params;
    const comments = await commentService.getByPostId(id);
    await reply.send(comments);
  };

  public static schema(): FastifySchema {
    return {
      tags: ['Comment'],
      params: {
        type: 'object',
        properties: {
          id: { type: 'number' }
        }
      },
      response: {
        200: {
          type: 'array',
          items: Comment,
          example: {
            id: 1123,
            postId: 1,
            content: 'My comment',
            author: 'john132'
          }
        },
        404: {
          type: 'object',
          properties: {
            message: { type: 'string' }
          }
        }
      }
    };
  }
}
