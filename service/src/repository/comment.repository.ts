import jsonData from './comment.json';

interface CommentJson {
  id: number;
  postId: number;
  content: string;
  createdAt: string; // Dates in JSON are usually strings
  author: string | null;
}

export interface Comment extends Omit<CommentJson, 'createdAt'> {
  createdAt: Date;
}

export default class CommentRepository {
  async findByPostId(postId: number): Promise<Comment[]> {
    return (jsonData as CommentJson[])
      .filter((i) => i.postId === postId)
      .map((item) => ({
        ...item,
        createdAt: new Date(item.createdAt)
      }));
  }

  async findById(id: number): Promise<Comment | null> {
    return (
      (jsonData as CommentJson[])
        .filter((i) => i.id === id)
        .map((item) => ({
          ...item,
          createdAt: new Date(item.createdAt)
        }))
        .pop() || null
    );
  }

  async countByPostId(id: number): Promise<number> {
    return (jsonData as CommentJson[]).filter((i) => i.postId === id).length;
  }
}
