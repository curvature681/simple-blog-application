"use client";
import useCommentsContext from "@hooks/useCommentsContext";
import { Loading } from "@ui";
import { useEffect } from "react";
import { CommentDetail } from "..";

type Props = {
  readonly postId: number;
};

export default function CommentsList({ postId }: Props) {
  const { comments, isLoading, fetchCommentForPost } = useCommentsContext();

  useEffect(() => {
    fetchCommentForPost(postId);
  }, [postId, fetchCommentForPost]);

  if (isLoading) {
    return <Loading />;
  }

  const postComments = comments.filter((c) => c.postId === postId);

  return (
    <>
      <h3>Comments</h3>
      {renderComments(postComments)}
    </>
  );
}

function renderComments(comments: ApiComment[]) {
  if (comments.length === 0) {
    return <p>No comments</p>;
  }

  return comments.map((comment) => (
    <CommentDetail key={comment.id} commentId={comment.id} />
  ));
}
