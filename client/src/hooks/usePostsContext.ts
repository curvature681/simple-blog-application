import { PostsContext } from "@/contexts/postsProvider";
import { useContext } from "react";

export default function usePostsContext() {
  const context = useContext(PostsContext);

  if (!context) {
    throw new Error("Missing posts context");
  }
  return context;
}
