import styles from "./not.found.error.module.css";

export default function NotFoundError() {
  return (
    <div className={styles.notFoundError}>
      <h2>SORRY</h2>
      <h3>We cannot find this content</h3>
    </div>
  );
}
