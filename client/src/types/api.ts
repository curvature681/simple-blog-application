interface ApiComment {
  id: number;
  postId: number;
  author: string | null;
  content: string;
}

interface ApiPost {
  id: number;
  title: string;
  content: string;
  comments: number;
}
