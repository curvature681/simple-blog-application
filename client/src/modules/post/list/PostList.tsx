"use client";
import { Loading } from "@ui";
import { PostTeaser } from "..";
import styles from "./post.list.module.css";
import usePostsContext from "@hooks/usePostsContext";

export default function PostList() {
  const { posts, isLoaded } = usePostsContext();

  if (!isLoaded) {
    return <Loading />;
  }

  if (posts.length === 0) {
    return <p>No blogs</p>;
  }

  return (
    <div className={styles.postList}>
      {posts.map((post) => (
        <PostTeaser id={post.id} key={post.id} />
      ))}
    </div>
  );
}
