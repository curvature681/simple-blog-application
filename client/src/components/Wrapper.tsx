import { Providers } from "@/components/Providers";
import { Layout } from "@ui";
import "@/app/globals.css";

export default function Wrapper({ children }: { children: React.ReactNode }) {
  return (
    <Layout>
      <Providers>{children}</Providers>
    </Layout>
  );
}
