import type { Meta, StoryObj } from "@storybook/react";

import HSpacer from "./HSpacer";
import { Providers } from "@/components/Providers";

const meta = {
  title: "UI/HSpacer",
  component: HSpacer,
  parameters: {
    layout: "centered",
  },
  decorators: [
    (Story) => (
      <Providers>
        <Story />
      </Providers>
    ),
  ],
  tags: ["autodocs"],
} satisfies Meta<typeof HSpacer>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    size: 1,
  },
};
