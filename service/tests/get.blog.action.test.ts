import Server from '@/config/server';
import { PostResponseType } from '@/controller/post/post.types';
import GetPostAction from '@/controller/post/get.post.action';

const server = new Server();

describe(GetPostAction.PATH, () => {
  beforeEach(() => {});

  afterEach(async () => {
    await server.get().close();
  });

  it('get all posts with correct structure', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'GET',
      url: '/api/post'
    });

    const data = response.json<PostResponseType[]>();
    expect(data.length).toEqual(3);
    expect(Object.keys(data[0]).length).toEqual(4);
    expect(data[0]).toEqual({
      id: 1,
      title: 'About the weather',
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      content: expect.any(String),
      comments: 3
    });
  });
});
