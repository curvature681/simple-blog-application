import type { Meta, StoryObj } from "@storybook/react";

import NotFoundError from "./NotFoundError";

const meta = {
  title: "UI/NotFoundError",
  component: NotFoundError,
  parameters: {
    layout: "centered",
  },

  tags: ["autodocs"],
} satisfies Meta<typeof NotFoundError>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
