import { render } from "@testing-library/react";
import CommentsList from "./CommentsList";
import useCommentsContext from "@hooks/useCommentsContext";
import { CommentDetailProps } from "../detail/CommentDetail";

jest.mock("@hooks/useCommentsContext");
jest.mock("..", () => ({
  CommentDetail: jest
    .fn()
    .mockImplementation(({ commentId }: CommentDetailProps) => {
      return <span>comment:{commentId}</span>;
    }),
}));

describe("CommentsList", () => {
  it("should render comments and call fetchCommentForPost on mount", () => {
    const mockComments: ApiComment[] = [
      { id: 1, postId: 1, author: "Author 1", content: "Content 1" },
      { id: 2, postId: 1, author: "Author 2", content: "Content 2" },
      { id: 3, postId: 3, author: "Author 3", content: "Content 3" }, // This comment should not be rendered
    ];
    const fetchCommentForPost = jest.fn();

    (useCommentsContext as jest.Mock).mockReturnValue({
      comments: mockComments,
      fetchCommentForPost,
    });

    const { getByText, queryByText } = render(<CommentsList postId={1} />);

    mockComments
      .filter((c) => c.postId === 1)
      .forEach((comment) => {
        getByText("comment:" + comment.id);
      });

    expect(queryByText("comment:3")).toBeNull();

    expect(fetchCommentForPost).toHaveBeenCalledWith(1);
  });
});
