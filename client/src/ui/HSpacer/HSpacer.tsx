import styles from "./hspacer.module.css";

type Props = {
  readonly size?: number;
};

export default function HSpacer({ size = 1 }: Props) {
  return <div className={styles.spacer} style={{ height: `${size}rem` }}></div>;
}
