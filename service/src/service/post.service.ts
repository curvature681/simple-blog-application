import { NotFound } from '@/error';
import CommentRepository from '@/repository/comment.repository';
import PostRepository, { Post } from '@/repository/post.repository';

export interface PostWithCommentCount extends Post {
  comments: number;
}

const postRepository = new PostRepository();
const commentRepository = new CommentRepository();

export class PostService {
  async findAll(): Promise<PostWithCommentCount[]> {
    const posts = await postRepository.findAll();
    const postsWithCommentCount = posts.map(async (post) => {
      const commentCount = await commentRepository.countByPostId(post.id);
      return {
        ...post,
        comments: commentCount
      };
    });
    return Promise.all(postsWithCommentCount);
  }

  /** @throws NotFound */
  async findById(id: number): Promise<PostWithCommentCount> {
    const post = await postRepository.findById(id);
    if (!post) {
      throw new NotFound('Post not found');
    }

    const commentCount = await commentRepository.countByPostId(id);
    return {
      ...post,
      comments: commentCount
    };
  }
}
