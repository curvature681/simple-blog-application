import { FastifySwaggerUiOptions } from '@fastify/swagger-ui';

const SWAGGER_URK = '/docs';

const fastifySwaggerUiOptions: FastifySwaggerUiOptions = {
  routePrefix: SWAGGER_URK,
  staticCSP: true,
  transformStaticCSP: (header) => header,
  uiConfig: {
    docExpansion: 'list',
    deepLinking: false
  }
};

export default fastifySwaggerUiOptions;
