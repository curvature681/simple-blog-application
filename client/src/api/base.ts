export default class BaseApi {
  protected static get baseUrl() {
    return process.env.API_URL || "http://localhost:8080/api";
  }

  protected static async get<T>(path: string): Promise<T> {
    try {
      const response = await fetch(BaseApi.baseUrl + path);
      return await response.json();
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
