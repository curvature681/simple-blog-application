"use client";

import usePostsContext from "@hooks/usePostsContext";
import { Box } from "@ui";

type Props = {
  id: number;
};

export default function PostTeaser({ id }: Props) {
  const { posts } = usePostsContext();

  const post = posts.find((post) => post.id === id);

  if (!post) {
    return <p>404</p>;
  }

  function renderNoComments(count: number): string {
    if (count === 0) {
      return "No comments";
    }

    if (count === 1) {
      return "1 comment";
    }

    return `${count} comments`;
  }

  return (
    <Box
      title={post.title}
      footer={renderNoComments(post.comments)}
      link={`/blog/${post.id}`}
    ></Box>
  );
}
