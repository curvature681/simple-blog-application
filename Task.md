## Coding challenge: Simple Blog Application

## Notes

_Please add your real name to README because its not alway obvious who submitted the challenge_

## Task

This application consists of a service and a client, they can be done in the same project or separately. There is no actual storage on the backend, so all the data can be mocked with some json.

### Service

The backend service should provide the following functionality:

Posts:

- list all

- get one

Comments:

- list all comments for a specific post

- create a new comment (user name is optional, text is required)

- update an existing comment

### Client

The client should display the following information:

- home page: list all blog post titles and show how many comments each blog post has

- blog post page: show text and all the posts' comments (It is not required to implement create/update actions for comments.)

### General remarks

- service: use Node.js with a framework of your choice

- client: use React.js or a framework of your choice

- provide unit tests to show your approach of testing both service and client

- use minimal CSS styling to show your approach for that
