import { render, waitFor } from "@testing-library/react";
import PostApi from "@/api/post";
import { PostsProvider, PostsContext } from "./postsProvider";
import { useContext } from "react";

jest.mock("@/api/post");

const TestComponent = () => {
  const context = useContext(PostsContext);

  if (!context) {
    return null;
  }

  const { posts, isLoaded } = context;
  return (
    <div>
      <div data-testid="isLoaded">{String(isLoaded)}</div>
      <div data-testid="posts">{JSON.stringify(posts)}</div>
    </div>
  );
};

describe("PostsProvider", () => {
  it("make API on load and loads correct state", async () => {
    const apiRespnse: ApiPost[] = [
      { id: 1, title: "Post 1", content: "Content 1", comments: 0 },
    ];
    PostApi.getPosts = jest.fn().mockResolvedValue(apiRespnse);

    const { getByTestId } = render(
      <PostsProvider>
        <TestComponent />
      </PostsProvider>
    );

    expect(getByTestId("isLoaded").textContent).toBe("false");
    // Wait for the posts to be loaded
    await waitFor(() =>
      expect(getByTestId("isLoaded").textContent).toBe("true")
    );

    expect(getByTestId("posts").textContent).toBe(JSON.stringify(apiRespnse));
    expect(PostApi.getPosts).toHaveBeenCalledTimes(1);
  });
});
