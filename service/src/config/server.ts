/* eslint-disable @typescript-eslint/no-floating-promises */
import fastifySwagger from '@fastify/swagger';
import fastifySwaggerUi from '@fastify/swagger-ui';
import fastifyCors from '@fastify/cors';
import { TypeBoxTypeProvider } from '@fastify/type-provider-typebox';
import fastify, { FastifyInstance } from 'fastify';
import fastifySwaggerUiOptions from './swagger-ui';
import { PORT } from '@/config/env';
import registerRoutes from './routes';

class Server {
  public server: FastifyInstance;
  constructor() {
    this.server = fastify({
      logger: true
    }).withTypeProvider<TypeBoxTypeProvider>();

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    this.init();
  }

  private async init(): Promise<void> {
    await this.registerPlugins();
    registerRoutes(this.server);
    await this.server.ready();
  }

  private async registerPlugins(): Promise<void> {
    await this.server.after();
    await this.server.register(fastifySwagger, {});
    await this.server.register(fastifySwaggerUi, fastifySwaggerUiOptions);
    await this.server.register(fastifyCors, {});
  }

  public listen(): void {
    const port = parseInt(PORT, 10);

    this.server.listen({ port: port, host: '0.0.0.0' }, (err, address) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }

      console.log(`Server listening at ${address}`);
    });
  }

  public get(): FastifyInstance {
    return this.server;
  }
}

export default Server;
