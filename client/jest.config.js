let nextJest = require("next/jest.js");

const createJestConfig = nextJest({
  dir: "./",
});

const customJestConfig = {
  testEnvironment: "jest-environment-jsdom",
  moduleDirectories: ["node_modules", "<rootDir>/"],
  moduleNameMapper: {
    "@hooks/(.*)": "<rootDir>/src/hooks/$1",
    "@ui": "<rootDir>/src/ui",
    "@/api/(.*)": "<rootDir>/src/api/$1",
  },
};

module.exports = createJestConfig(customJestConfig);
