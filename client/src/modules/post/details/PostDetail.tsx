"use client";

import { Header, NotFoundError } from "@ui";
import styles from "./post.details.module.css";
import usePostsContext from "@hooks/usePostsContext";

type Props = {
  readonly id: number;
};

export default function PostDetail({ id }: Props) {
  const { posts, isLoaded } = usePostsContext();

  const post = posts.find((post) => post.id === id);

  if (!post && isLoaded) {
    return <NotFoundError />;
  }

  if (!post) {
    return;
  }

  return (
    <div>
      <Header title={post.title} />

      <p className={styles.content}>{post.content}</p>
    </div>
  );
}
