"use client";

import { CommentsProvider } from "@/contexts/commentsProvider";
import { PostsProvider } from "@/contexts/postsProvider";

type Props = React.PropsWithChildren;

export function Providers({ children }: Props) {
  return (
    <PostsProvider>
      <CommentsProvider>{children}</CommentsProvider>
    </PostsProvider>
  );
}
