/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import Server from '@/config/server';
import AddCommentAction from '@/controller/comment/add.comment.action';
import { CommentType, NewCommentType } from '@/controller/comment/comment.types';
import { randomString } from '@/tests/helpers';
import { equal } from 'assert';

const server = new Server();

type ErrorResponse = {
  message: string;
};

describe(AddCommentAction.PATH, () => {
  beforeEach(() => {});

  afterEach(async () => {
    await server.get().close();
  });

  it('Adds correct comments without author', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'POST',
      url: '/api/post/1/comment',
      payload: {
        content: 'My comment'
      } as NewCommentType
    });

    const data = response.json<CommentType>();
    expect(Object.keys(data).length).toEqual(4);
    expect(data).toEqual({
      id: expect.any(Number),
      postId: 1,
      content: 'My comment',
      author: ''
    } as CommentType);
  });

  it('Adds correct comments with author', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'POST',
      url: '/api/post/1/comment',
      payload: {
        content: 'My comment',
        author: 'john132'
      } as NewCommentType
    });

    const data = response.json<CommentType>();
    expect(Object.keys(data).length).toEqual(4);
    expect(data).toEqual({
      id: expect.any(Number),
      postId: 1,
      content: 'My comment',
      author: 'john132'
    } as CommentType);
  });

  it('fails on to large comment', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'POST',
      url: '/api/post/1/comment',
      payload: {
        content: randomString(1001),
        author: 'john132'
      } as NewCommentType
    });

    const data = response.json<ErrorResponse>();
    equal(response.statusCode, 400);
    equal(data.message, 'body/content must NOT have more than 1000 characters');
  });

  it('fails on to a long author name', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'POST',
      url: '/api/post/1/comment',
      payload: {
        content: 'OK',
        author: randomString(256)
      } as NewCommentType
    });

    const data = response.json<ErrorResponse>();
    equal(response.statusCode, 400);
    equal(data.message, 'body/author must NOT have more than 255 characters');
  });

  it('fails on wrong post id', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'POST',
      url: '/api/post/123123/comment',
      payload: {
        content: 'OK',
        author: 'pl'
      } as NewCommentType
    });

    const data = response.json<ErrorResponse>();
    equal(response.statusCode, 404);
    equal(data.message, 'Comment not found');
  });
});
