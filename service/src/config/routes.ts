import GetPostAction from '@/controller/post/get.post.action';
import GetPostByIdAction from '@/controller/post/get.postbyId.action';
import AddCommentAction from '@/controller/comment/add.comment.action';
import GetCommentByPostAction from '@/controller/comment/get.commentByPost.action';
import UpdateCommentAction from '@/controller/comment/update.comment.action';
import { FastifyInstance } from 'fastify';

const API_PREFIX = '/api';

export default function registerRoutes(server: FastifyInstance): void {
  server.get(API_PREFIX + GetPostAction.PATH, { schema: GetPostAction.schema() }, GetPostAction.action);
  server.get(API_PREFIX + GetPostByIdAction.PATH, { schema: GetPostByIdAction.schema() }, GetPostByIdAction.action);
  // comments
  server.post(
    API_PREFIX + AddCommentAction.PATH,
    {
      schema: AddCommentAction.schema()
    },
    AddCommentAction.action
  );
  server.get(
    API_PREFIX + GetCommentByPostAction.PATH,
    {
      schema: GetCommentByPostAction.schema()
    },
    GetCommentByPostAction.action
  );
  server.patch(
    API_PREFIX + UpdateCommentAction.PATH,
    {
      schema: UpdateCommentAction.schema()
    },
    UpdateCommentAction.action
  );
}
