import styles from "./header.module.css";

type Props = {
  readonly title: string;
};

export default function Box({ title }: Props) {
  return (
    <div className={styles.header}>
      <h1>{title}</h1>
    </div>
  );
}
