module.exports = {
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  coverageReporters: ['text', 'lcov', 'text-summary'],
  collectCoverageFrom: ['src/**/*.ts'],
  modulePathIgnorePatterns: ['<rootDir>/dist/']
};
