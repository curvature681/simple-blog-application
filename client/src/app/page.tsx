import { PostList } from "@/modules/post";
import { Header } from "@ui";

export default function Home() {
  return (
    <>
      <Header title="Awesome Blog" />
      <PostList />
    </>
  );
}
