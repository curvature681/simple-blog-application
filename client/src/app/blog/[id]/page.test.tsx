import {
  fireEvent,
  render,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import Blog from "./page";
import Wrapper from "@/components/Wrapper";
import PostApi from "@/api/post";
import CommentApi from "@/api/comment";
import { RouterContext } from "next/dist/shared/lib/router-context.shared-runtime";
import { NextRouter } from "next/router";

jest.mock("@/api/post");
jest.mock("@/api/comment");
const router: Partial<NextRouter> = {
  push: jest.fn(),
  prefetch: jest.fn(),
};

describe("Blog", () => {
  it("shows 404", async () => {
    PostApi.getPosts = jest.fn().mockReturnValue([]);
    CommentApi.getCommentsByPost = jest.fn().mockReturnValue([]);
    const { getByText, queryByText, container } = render(
      <Wrapper>
        <Blog params={{ id: "123123" }} />
      </Wrapper>
    );

    await waitForElementToBeRemoved(() => container.querySelector(".loader"));

    await waitFor(() => {
      queryByText("SORRY");
    });
    expect(CommentApi.getCommentsByPost).toHaveBeenCalledTimes(1);
    expect(getByText("SORRY")).toBeDefined();
    expect(getByText("No comments")).toBeDefined();
  });

  it("clicks on go back", async () => {
    PostApi.getPosts = jest.fn().mockReturnValue([]);
    CommentApi.getCommentsByPost = jest.fn().mockReturnValue([]);
    const { getByText, queryByText } = render(
      <RouterContext.Provider value={router as NextRouter}>
        <Wrapper>
          <Blog params={{ id: "3233" }} />
        </Wrapper>
      </RouterContext.Provider>
    );

    await waitFor(() => {
      queryByText("SORRY");
    });

    const goBackLink = getByText("GO BACK");
    expect(goBackLink).toBeDefined();

    fireEvent.click(goBackLink);
    expect(router.push).toHaveBeenCalledWith("/", expect.any(Object));
  });

  it("renders blog and comments", async () => {
    PostApi.getPosts = jest
      .fn()
      .mockReturnValue([
        { id: 1, title: "Title 1", content: "Content 1", comments: 0 },
      ]);
    CommentApi.getCommentsByPost = jest
      .fn()
      .mockReturnValue([
        { id: 1, postId: 1, content: "Comment 1", author: "Author 1" },
      ] as ApiComment[]);
    const { getByText, queryByText, debug } = render(
      <RouterContext.Provider value={router as NextRouter}>
        <Wrapper>
          <Blog params={{ id: "1" }} />
        </Wrapper>
      </RouterContext.Provider>
    );

    await waitFor(() => {
      queryByText("Title 1");
    });

    expect(getByText("Title 1")).toBeDefined();
    expect(getByText("Content 1")).toBeDefined();
  });
});
