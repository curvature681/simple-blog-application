import jsonData from './post.json';

type PostJson = {
  id: number;
  title: string;
  content: string;
  createdAt: string; // Dates in JSON are usually strings
  author: string;
};

export type Post = {
  id: number;
  title: string;
  content: string;
  createdAt: Date;
  author: string;
};

export default class PostRepository {
  async findAll(): Promise<Post[]> {
    return (jsonData as PostJson[]).map((item) => ({
      ...item,
      createdAt: new Date(item.createdAt)
    }));
  }

  async findById(id: number): Promise<Post | null> {
    return (
      (jsonData as PostJson[])
        .filter((i) => i.id === id)
        .map((item) => ({
          ...item,
          createdAt: new Date(item.createdAt)
        }))
        .pop() || null
    );
  }
}
