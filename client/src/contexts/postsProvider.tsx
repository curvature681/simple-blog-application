import PostApi from "@/api/post";
import {
  ReactNode,
  createContext,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";

export interface PostsContextType {
  posts: ApiPost[];
  isLoaded: boolean;
  fetchApiPostById: (id: number) => Promise<ApiPost | null>;
}

export const PostsContext = createContext<PostsContextType | undefined>(
  undefined
);

export const PostsProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [posts, setPosts] = useState<ApiPost[]>([]);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  useEffect(() => {
    const fetchApiPosts = async () => {
      setIsLoaded(false);
      try {
        const data = await PostApi.getPosts();
        setPosts(data);
      } catch (error) {
        console.error("Error fetching blog posts:", error);
      } finally {
        setIsLoaded(true);
      }
    };

    fetchApiPosts();
  }, []);

  const fetchApiPostById = useCallback(
    async (id: number): Promise<ApiPost | null> => {
      try {
        const data = await PostApi.getPost(id);

        if (data) {
          setPosts(posts.map((post) => (post.id === id ? data : post)));
        }
        return data;
      } catch (error) {
        return null;
      }
    },
    [posts]
  );

  const contextValue = useMemo(
    () => ({ posts, isLoaded, fetchApiPostById }),
    [posts, isLoaded, fetchApiPostById]
  );

  return (
    <PostsContext.Provider value={contextValue}>
      {children}
    </PostsContext.Provider>
  );
};
