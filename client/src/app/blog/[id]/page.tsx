import { CommentsList } from "@/modules/comment";
import { PostDetail } from "@/modules/post";
import { HSpacer, Link } from "@ui";

export default function Home({ params }: { params: { id: string } }) {
  const postId = parseInt(params.id, 10);
  return (
    <>
      <Link href="/" text="GO BACK" />
      <HSpacer size={2} />
      <PostDetail id={postId} />

      <HSpacer size={2} />

      <CommentsList postId={postId} />
    </>
  );
}
