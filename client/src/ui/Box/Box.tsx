import Link from "next/link";
import styles from "./box.module.css";

type Props = {
  title?: string;
  content?: string;
  footer?: string;
  link?: string;
};

export default function Box({ content, title, footer, link }: Props) {
  function wrap(children: React.ReactNode) {
    if (link) {
      return (
        <Link href={link} className={styles.box}>
          {children}
        </Link>
      );
    }
    return <div className={styles.box}>{children}</div>;
  }

  return wrap(
    <>
      {title ? <h3>{title}</h3> : null}
      {content ? <p>{content}</p> : null}
      {footer ? <span>{footer}</span> : null}
    </>
  );
}
