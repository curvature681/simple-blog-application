import BaseApi from "./base";

export default class PostApi extends BaseApi {
  static getPosts(): Promise<ApiPost[]> {
    return BaseApi.get<ApiPost[]>("/post");
  }

  static getPost(id: number): Promise<ApiPost | null> {
    return BaseApi.get<ApiPost>("/post/" + id);
  }
}
