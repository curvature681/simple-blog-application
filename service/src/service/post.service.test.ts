const mockedPostfindById = jest.fn();
const mockedPostFindAll = jest.fn();
const mockedCommentCountByPostId = jest.fn();

jest.mock('@/repository/post.repository', () => ({
  __esModule: true,
  default: jest.fn().mockImplementation(() => {
    return {
      findById: mockedPostfindById,
      findAll: mockedPostFindAll
    };
  })
}));

jest.mock('@/repository/comment.repository', () => ({
  __esModule: true,
  default: jest.fn().mockImplementation(() => {
    return {
      countByPostId: mockedCommentCountByPostId
    };
  })
}));

import { NotFound } from '@/error';
import { PostService } from './post.service';

describe('PostService', () => {
  let postService: PostService;

  beforeEach(() => {
    postService = new PostService();
  });

  it('should return all posts', async () => {
    const mockPosts = [
      {
        id: 1,
        title: 'Test Post',
        content: 'This is a test post',
        createdAt: new Date(),
        author: 'Test Author'
      },
      {
        id: 2,
        title: 'Another Test Post',
        content: 'This is another test post',
        createdAt: new Date(),
        author: 'Another Test Author'
      }
    ];
    mockedPostFindAll.mockResolvedValue(mockPosts);
    mockedCommentCountByPostId.mockResolvedValue(1);
    const posts = await postService.findAll();
    expect(posts).toEqual(mockPosts.map((a) => ({ ...a, comments: 1 })));
  });

  it('should return a post for a given id', async () => {
    const mockPost = {
      id: 1,
      title: 'Test Post',
      content: 'This is a test post',
      createdAt: new Date(),
      author: 'Test Author'
    };
    mockedPostfindById.mockResolvedValue(mockPost);
    mockedCommentCountByPostId.mockResolvedValue(1);

    const post = await postService.findById(1);
    expect(post).toEqual({
      ...mockPost,
      comments: 1
    });
  });

  it('should throw NotFound when no post is found for a given id', async () => {
    mockedPostfindById.mockResolvedValue(null);

    await expect(postService.findById(999)).rejects.toThrow(NotFound);
  });
});
