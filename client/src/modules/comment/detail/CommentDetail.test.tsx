import { render } from "@testing-library/react";
import CommentDetail from "./CommentDetail";
import useCommentsContext from "@hooks/useCommentsContext";

jest.mock("@hooks/useCommentsContext");

describe("CommentDetail", () => {
  it("renders the comment with the given id", () => {
    const commentId = 1;
    const comment = {
      id: commentId,
      author: "Author",
      content: "Content",
    };

    (useCommentsContext as jest.Mock).mockReturnValue({
      comments: [comment],
    });

    const { getByText } = render(<CommentDetail commentId={commentId} />);

    expect(getByText(comment.author)).toBeDefined();
    expect(getByText(comment.content)).toBeDefined();
  });

  it("doesnt render for unknown comment", () => {
    const commentId = 1;

    (useCommentsContext as jest.Mock).mockReturnValue({
      comments: [],
    });

    const { container } = render(<CommentDetail commentId={commentId} />);

    expect(container.innerHTML).toBe("");
  });
});
