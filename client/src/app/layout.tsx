import type { Metadata } from "next";
import Wrapper from "@/components/Wrapper";
export const metadata: Metadata = {
  title: "Awesome Blog",
  description: "The power of journalism",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <Wrapper>{children}</Wrapper>
      </body>
    </html>
  );
}
