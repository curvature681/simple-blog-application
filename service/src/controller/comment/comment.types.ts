import { Static, Type } from '@sinclair/typebox';

const RequestIdParam = Type.Object({
  id: Type.Number()
});
export type RequestIdParamType = Static<typeof RequestIdParam>;

export const NewComment = Type.Object({
  content: Type.String({
    minLength: 1,
    maxLength: 1000
  }),
  author: Type.Optional(
    Type.String({
      minLength: 1,
      maxLength: 255
    })
  )
});

export type NewCommentType = Static<typeof NewComment>;

export const Comment = Type.Object({
  id: Type.Number(),
  postId: Type.Number(),
  content: Type.String(),
  author: Type.Optional(Type.String())
});
export type CommentType = Static<typeof Comment>;
