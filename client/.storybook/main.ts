import type { StorybookConfig } from "@storybook/nextjs";
import path from "path";

const config: StorybookConfig = {
  stories: ["../src/**/*.mdx", "../src/**/*.stories.@(js|jsx|mjs|ts|tsx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-onboarding",
    "@storybook/addon-interactions",
    "storybook-addon-mock",
  ],
  framework: {
    name: "@storybook/nextjs",
    options: {},
  },
  docs: {
    autodocs: "tag",
  },
  webpackFinal: async (config) => {
    config.resolve = config.resolve || {};
    config.resolve.alias = config.resolve.alias || {};
    config.resolve.alias = {
      ...config.resolve.alias,
      "@/modules": path.resolve(__dirname, "../src/modules"),
      "@ui": path.resolve(__dirname, "../src/ui"),
      "@hooks": path.resolve(__dirname, "../src/hooks"),
      "@/contexts": path.resolve(__dirname, "../src/contexts"),
      "@/api": path.resolve(__dirname, "../src/api"),
      "@/components": path.resolve(__dirname, "../src/components"),
      "@/app": path.resolve(__dirname, "../src/app"),
    };

    return config;
  },
};
export default config;
