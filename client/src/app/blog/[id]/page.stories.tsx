import type { Meta, StoryObj } from "@storybook/react";

import Page from "./page";
import Wrapper from "@/components/Wrapper";

const meta = {
  title: "Pages/Blog",
  component: Page,
  parameters: {
    mockData: [
      {
        url: "http://localhost:8080/api/post",
        method: "GET",
        status: 200,
        response: [
          {
            id: 1,
            title: "Hello World",
            content: "This is a test post",
            comments: 0,
          },
        ] as ApiPost[],
      },
      {
        url: "http://localhost:8080/api/post/1/comment",
        method: "GET",
        status: 200,
        response: [
          {
            id: 1,
            postId: 1,
            author: "John",
            content: "This is a test post",
          },
        ] as ApiComment[],
      },
    ],
  },
  decorators: [
    (Story) => (
      <Wrapper>
        <Story />
      </Wrapper>
    ),
  ],

  tags: ["autodocs"],
} satisfies Meta<typeof Page>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    params: {
      id: "1",
    },
  },
};
