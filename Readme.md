## Author

![Me](/me.jpg)

## Assumption / Decisions

1. I didn't use backend capability of NextJS to separate service and the client.

2. For service I chosen [fastify](https://fastify.dev/)

   - simple and performant
   - Have many plugins needed for building REST API

3. For API approach I chosen RESTFull API, which will keep both services and client simple.
4. I put all in single repository (service, client, e2e)
5. Created or updated comments are not saved, but returned by the API
6. I used Next.js without server side functions to have all in one package (React, Routing, SSR)
7. I use ReactContext as store and to run side effects to keep the dependecies minimal

## Task

Task description is available [here](Task.md)

## Links

[GitLab - CI](https://gitlab.com/curvature681/simple-blog-application/-/pipelines) \
[GitLab - Repository](https://gitlab.com/curvature681/simple-blog-application)\
[SonnarCloud](https://sonarcloud.io/project/overview?id=curvature681_simple-blog-application)

## Running the project

Running the project from the root folder. Make sure ports `3000` and `8080` are not used

### Service

```bash
cd service
npm i
npm run dev # npm run start
```

### Client

_make sure service is running as well_

```bash
cd client
npm i
npm run dev # npm run build && npm run start
```

Alternativally you can run it from the **docker**

```bash
docker-compose up
```

Service will run start on port `8080` and on local url [http://localhost:8080](http://localhost:8080)

After starting the service to check the swagger documentation [http://localhost:8080/docs](http://localhost:8080/docs)

Client will open on port `3000` and on local url [http://localhost:3000](http://localhost:3000)

### Tests
Run unit tests, integrations tests, the test coverage per service can be done

```bash
cd client # cd service
npm i
npm run test 
npm run coverage 
```

### Storybook
Chcecking visually components with [storybook](https://storybook.js.org/) Should automatically open [http://localhost:6006/](http://localhost:6006/)

```bash
cd client
npm i
npm run storybook
```

### E2E tests
Running end to end tests

_make sure service and client are running, either separatelly or via docker_

```bash
cd e2e
npm i
npx playwright install --with-deps
npx playwright test  # runs the tests
npx playwright test --ui # opens Test GUI manager
```

## TODO

[x] service \
&emsp; [x] basic project structure \
&emsp; [x] Eslint & Prietter \
&emsp; [x] aliases \
&emsp; [x] Data model structure / repositories \
&emsp; [x] Swagger \
&emsp; [x] API endpoints (5) \
&emsp; [x] API response models (5) \
&emsp; [x] API validators (POST|PATCH) \
&emsp; [x] Unit tests \
&emsp; [x] Integration tests (APIs) \
&emsp; [x] Readme (run/test) \
[x] client \
&emsp; [x] basic project structure (consider routing for framework) \
&emsp; [x] css approach \
&emsp; [x] UI \
&emsp; [x] home page \
&emsp; [x] blog post page \
&emsp; [x] check responsivness \
&emsp; [x] Design more appling \
&emsp; [x] Unit tests \
&emsp; [x] integration tests \
&emsp; [x] Storybook \
&emsp; [x] CSS to use var \
[x] e2e \
&emsp; [x] choose solution \
&emsp; [x] implement test cases \
[x] others \
&emsp; [x] Dockerize app \
&emsp; [x] Cleanup readme \
&emsp; [x] CI \
&emsp; [x] npx tsc \
&emsp; [x] *SonnarCloud \
&emsp; [x] *GitLab CI \
&emsp; [x] \*text typos \
[x] Add signature
