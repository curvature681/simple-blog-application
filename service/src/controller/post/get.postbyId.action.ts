import { PostService } from '@/service/post.service';
import { FastifyReply, FastifyRequest, FastifySchema } from 'fastify';
import { PostsResponseType, RequestIdParamType, PostResponse } from './post.types';

const postService = new PostService();

export default class GetPostByIdAction {
  static readonly PATH = '/post/:id';

  public static action = async (
    request: FastifyRequest<{ Params: RequestIdParamType; Reply: PostsResponseType }>,
    reply: FastifyReply
  ) => {
    const { id } = request.params;
    const post = await postService.findById(id);
    await reply.send(post);
  };

  public static schema(): FastifySchema {
    return {
      tags: ['Post'],
      params: {
        type: 'object',
        properties: {
          id: { type: 'number' }
        }
      },
      response: {
        200: {
          ...PostResponse,
          example: {
            id: 1,
            title: 'Test Post',
            content: 'This is a test post'
          }
        },
        404: {
          type: 'object',
          properties: {
            message: { type: 'string' }
          }
        }
      }
    };
  }
}
