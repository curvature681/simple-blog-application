import CommentApi from "@/api/comment";

import {
  ReactNode,
  createContext,
  useCallback,
  useMemo,
  useState,
} from "react";

export interface CommentsContextType {
  comments: ApiComment[];
  isLoading: boolean;
  fetchCommentForPost: (postId: number) => Promise<ApiComment[] | null>;
}

export const CommentsContext = createContext<CommentsContextType | undefined>(
  undefined
);

const loadedPosts: number[] = [];

export const CommentsProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [comments, setComments] = useState<ApiComment[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const fetchCommentForPost = useCallback(
    async (postId: number): Promise<ApiComment[] | null> => {
      if (loadedPosts.includes(postId)) {
        return comments.filter((c) => c.postId === postId);
      }
      try {
        setIsLoading(true);
        const data = await CommentApi.getCommentsByPost(postId);

        const commentsToAdd = data.filter(
          (c) => !comments.find((c2) => c2.id === c.id)
        );
        setComments([...comments, ...commentsToAdd]);
        loadedPosts.push(postId);

        return data;
      } catch (error) {
        return null;
      } finally {
        setIsLoading(false);
      }
    },
    [comments]
  );

  const contextValue = useMemo(
    () => ({ comments, isLoading, fetchCommentForPost }),
    [comments, isLoading, fetchCommentForPost]
  );

  return (
    <CommentsContext.Provider value={contextValue}>
      {children}
    </CommentsContext.Provider>
  );
};
