import CommentsList from "./list/CommentsList";
import CommentDetail from "./detail/CommentDetail";

export { CommentsList, CommentDetail };
