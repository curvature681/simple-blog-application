import NextLink from "next/link";
import styles from "./link.module.css";

type Props = {
  readonly href: string;
  readonly text: string;
};

export default function Link({ href, text }: Props) {
  return (
    <NextLink href={href} className={styles.link}>
      {text}
    </NextLink>
  );
}
