import { NextRouter } from "next/router";

export const router: Partial<NextRouter> = {
  push: jest.fn(),
  prefetch: jest.fn(),
};
