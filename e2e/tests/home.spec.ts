import { test, expect } from "@playwright/test";

test("Test home Page", async ({ page }) => {
  await page.goto(process.env.CLIENT_URL as string);
  await expect(
    page.getByRole("heading", { name: "Awesome Blog" })
  ).toBeVisible();
  await page.getByRole("link", { name: "About the weather" }).click();

  await page.waitForURL("**/blog/1");
  await expect(
    page.getByRole("heading", { name: "About the weather" })
  ).toBeVisible();
});
