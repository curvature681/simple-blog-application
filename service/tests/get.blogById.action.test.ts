import Server from '@/config/server';
import { PostResponseType } from '@/controller/post/post.types';
import GetPostByIdAction from '@/controller/post/get.postbyId.action';

const server = new Server();

describe(GetPostByIdAction.PATH, () => {
  beforeEach(() => {});

  afterEach(async () => {
    await server.get().close();
  });

  it('gets correct response', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'GET',
      url: '/api/post/1'
    });

    const data = response.json<PostResponseType>();
    expect(Object.keys(data).length).toEqual(4);
    expect(data).toEqual({
      id: 1,
      title: 'About the weather',
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      content: expect.any(String),
      comments: 3
    });
  });

  it('return 404 on not valid ', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'GET',
      url: '/api/post/1223123'
    });

    expect(response.statusCode).toEqual(404);
  });
});
