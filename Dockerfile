FROM node:20-alpine

WORKDIR /app

COPY --chown=node --chmod=0755 . .

RUN npm install --ignore-scripts

USER node
CMD ["npm", "run", "dev"]
