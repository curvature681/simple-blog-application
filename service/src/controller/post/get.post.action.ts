import { PostService } from '@/service/post.service';
import { FastifyReply, FastifyRequest, FastifySchema } from 'fastify';
import { PostResponse, PostsResponseType } from './post.types';

const postService = new PostService();

export default class GetPostAction {
  static readonly PATH = '/post';

  public static action = async (request: FastifyRequest<{ Reply: PostsResponseType }>, reply: FastifyReply) => {
    const data = await postService.findAll();
    await reply.send(data);
  };

  public static schema(): FastifySchema {
    return {
      tags: ['Post'],
      response: {
        200: {
          type: 'array',
          items: PostResponse,
          example: [
            {
              id: 1,
              title: 'Test Post',
              content: 'This is a test post',
              comments: 0
            }
          ]
        }
      }
    };
  }
}
