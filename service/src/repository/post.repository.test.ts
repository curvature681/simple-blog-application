import PostRepository, { Post } from './post.repository';

describe('PostRepository', () => {
  let postRepository: PostRepository;

  beforeEach(() => {
    postRepository = new PostRepository();
  });

  it('should return an array of Posts', async () => {
    const Posts: Post[] = await postRepository.findAll();
    expect(Array.isArray(Posts)).toBe(true);
    expect(Posts.length).toBeGreaterThan(0);
  });

  it('should return Posts with the correct properties', async () => {
    const Posts: Post[] = await postRepository.findAll();
    const firstPost = Posts[0];
    expect(firstPost).toHaveProperty('id');
    expect(firstPost).toHaveProperty('title');
    expect(firstPost).toHaveProperty('content');
    expect(firstPost).toHaveProperty('createdAt');
    expect(firstPost['createdAt']).toBeInstanceOf(Date);
    expect(firstPost).toHaveProperty('author');
  });

  it('should return the correct Post when given an id', async () => {
    const Post = (await postRepository.findById(1)) as Post;
    expect(Post).toBeDefined();

    expect(Post.id).toBe(1);
    expect(Post.title).toBe('About the weather');
    expect(Post.content.length).toBeGreaterThan(255);
    expect(Post.createdAt).toBeInstanceOf(Date);
    expect(Post.author).toBe('John Doe');
  });

  it('should return null when given an id that does not exist', async () => {
    const Post = await postRepository.findById(3333);
    expect(Post).toBeNull();
  });
});
