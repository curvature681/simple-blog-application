import type { Meta, StoryObj } from "@storybook/react";

import Box from "./Box";
const meta = {
  title: "UI/Box",
  component: Box,
  parameters: {
    layout: "centered",
  },

  tags: ["autodocs"],
} satisfies Meta<typeof Box>;

export default meta;
type Story = StoryObj<typeof meta>;

export const AsLinkDefault: Story = {
  args: {
    title: "This is a box title",
    content: "Box Content content of the box title",
    link: "/",
  },
};

export const Default: Story = {
  args: {
    title: "This is a box title",
    content: "Box Content content of the box title",
  },
};

export const WithFooter: Story = {
  args: {
    title: "This is a box title",
    content: "Box Content content of the box title",
    footer: "footer content",
  },
};

export const Content: Story = {
  args: {
    content: "This is a content only",
  },
};
