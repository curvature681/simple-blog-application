import { CommentsContext } from "@/contexts/commentsProvider";
import { useContext } from "react";

export default function useCommentsContext() {
  const context = useContext(CommentsContext);

  if (!context) {
    throw new Error("Missing posts context");
  }
  return context;
}
