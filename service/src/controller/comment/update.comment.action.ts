import { FastifyReply, FastifyRequest, FastifySchema } from 'fastify';
import { CommentType, RequestIdParamType, Comment, NewCommentType, NewComment } from './comment.types';
import { CommentService } from '@/service/comment.service';

const commentService = new CommentService();

export default class UpdateCommentAction {
  static readonly PATH = '/comment/:id';

  public static action = async (
    request: FastifyRequest<{ Params: RequestIdParamType; Body: NewCommentType; Reply: CommentType }>,
    reply: FastifyReply
  ) => {
    const { id } = request.params;
    const comment = await commentService.updateComment(id, request.body);
    await reply.send(comment);
  };

  public static schema(): FastifySchema {
    return {
      tags: ['Comment'],
      body: NewComment,
      params: {
        type: 'object',
        properties: {
          id: { type: 'number' }
        }
      },
      response: {
        200: {
          ...Comment,
          example: {
            id: 1123,
            postId: 1,
            content: 'My comment',
            author: 'john132'
          }
        },
        404: {
          type: 'object',
          properties: {
            message: { type: 'string' }
          }
        }
      }
    };
  }
}
