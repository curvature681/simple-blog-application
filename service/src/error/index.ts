export class NotFound extends Error {
  statusCode: number;

  constructor(message = 'Not found') {
    super(message);
    this.statusCode = 404;
  }
}
