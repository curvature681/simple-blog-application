import { test, expect } from "@playwright/test";

test("Blog Page", async ({ page }) => {
  await page.goto((process.env.CLIENT_URL as string) + "blog/1");
  await expect(
    page.getByRole("heading", { name: "About the weather" })
  ).toBeVisible();

  await expect(page.getByRole("heading", { name: "Comments" })).toBeVisible();
  await expect(page.getByText("I love that post post")).toBeVisible();
  await expect(page.getByText("John!I hate that post post")).toBeVisible();
  await expect(page.getByRole("heading", { name: "Martin" })).toBeVisible();
});

test("Blog Page navigate to home", async ({ page }) => {
  await page.goto((process.env.CLIENT_URL as string) + "blog/1");
  await expect(
    page.getByRole("heading", { name: "About the weather" })
  ).toBeVisible();

  await page.getByRole("link", { name: "GO BACK" }).click();
  await page.waitForURL("**/");
  await expect(
    page.getByRole("heading", { name: "Awesome Blog" })
  ).toBeVisible();
});
