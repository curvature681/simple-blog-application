/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import Server from '@/config/server';

const server = new Server();

type SwaggerJson = {
  paths: Record<string, unknown>;
};

describe('Swagger UI', () => {
  beforeEach(() => {});

  afterEach(async () => {
    await server.get().close();
  });

  it('renders UI', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'GET',
      url: '/docs/static/index.html'
    });
    console.log(response.payload);
    const data = response.payload;

    expect(data).toContain('Swagger UI');
  });

  it('return swagger JSON', async () => {
    const server = new Server();
    await server.get().ready();
    const response = await server.get().inject({
      method: 'GET',
      url: '/docs/json'
    });

    const data = response.json<SwaggerJson>();

    expect(Object.keys(data?.paths).length).toBeGreaterThan(3);
  });
});
