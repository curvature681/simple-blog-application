import BaseApi from "./base";

export default class CommentApi extends BaseApi {
  static getCommentsByPost(postId: number): Promise<ApiComment[]> {
    return BaseApi.get<ApiComment[]>(`/post/${postId}/comment`);
  }
}
