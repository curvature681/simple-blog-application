import CommentRepository from './comment.repository';
import jsonData from './comment.json';

jest.mock('./comment.json', () => [
  {
    id: 1,
    postId: 1,
    content: 'Test Comment',
    createdAt: '2022-01-01T00:00:00Z',
    author: 'Test Author'
  },
  {
    id: 2,
    postId: 1,
    content: 'Another Test Comment',
    createdAt: '2022-01-01T00:00:00Z',
    author: 'Another Test Author'
  },
  {
    id: 3,
    postId: 2,
    content: 'Test Comment 2',
    createdAt: '2022-01-01T00:00:00Z',
    author: 'Test Author 2'
  }
]);

describe('CommentRepository', () => {
  let commentRepository: CommentRepository;

  beforeEach(() => {
    commentRepository = new CommentRepository();
  });

  it('should return comments for a given post id', async () => {
    const comments = await commentRepository.findByPostId(1);
    expect(comments.length).toBe(2);
    expect(comments[0].id).toBe(jsonData[0].id);
    expect(comments[1].id).toBe(jsonData[1].id);
  });

  it('should return a comment for a given id', async () => {
    const comment = await commentRepository.findById(1);
    expect(comment).toEqual({
      ...jsonData[0],
      createdAt: new Date(jsonData[0].createdAt)
    });
  });

  it('should return null when no comment is found for a given id', async () => {
    const comment = await commentRepository.findById(999);
    expect(comment).toBeNull();
  });
});
