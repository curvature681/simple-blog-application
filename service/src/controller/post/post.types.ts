import { Static, Type } from '@sinclair/typebox';

export const PostResponse = Type.Object({
  id: Type.Number(),
  title: Type.String(),
  content: Type.String(),
  comments: Type.Number()
});

export const PostsResponse = Type.Array(PostResponse);
export type PostResponseType = Static<typeof PostResponse>;
export type PostsResponseType = Static<typeof PostsResponse>;

const RequestIdParam = Type.Object({
  id: Type.Number()
});
export type RequestIdParamType = Static<typeof RequestIdParam>;
