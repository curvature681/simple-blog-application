import {
  fireEvent,
  render,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import Home from "./page";
import Wrapper from "@/components/Wrapper";
import PostApi from "@/api/post";
import { RouterContext } from "next/dist/shared/lib/router-context.shared-runtime";
import { NextRouter } from "next/router";

jest.mock("@/api/post");
const router: Partial<NextRouter> = {
  push: jest.fn(),
  prefetch: jest.fn(),
};

describe("Home", () => {
  it("It shows no blogs", async () => {
    PostApi.getPosts = jest.fn().mockReturnValue([]);
    const { getByText, container } = render(
      <Wrapper>
        <Home />
      </Wrapper>
    );

    await waitForElementToBeRemoved(() => container.querySelector(".loader"));

    expect(getByText("No blogs")).toBeDefined();
  });

  it("blogs are clickable", async () => {
    const posts: ApiPost[] = [
      { id: 1, title: "Title 1", content: "Content 1", comments: 0 },
    ];
    PostApi.getPosts = jest.fn().mockReturnValue(posts);
    const { getByText, container } = render(
      <RouterContext.Provider value={router as NextRouter}>
        <Wrapper>
          <Home />
        </Wrapper>
      </RouterContext.Provider>
    );
    await waitForElementToBeRemoved(() => container.querySelector(".loader"));

    const blog1Title = getByText("Title 1");
    expect(blog1Title).toBeDefined();

    const blog1Link = blog1Title.closest("a");
    if (!blog1Link) {
      throw new Error("blog1Link is null");
    }

    fireEvent.click(blog1Link);
    expect(router.push).toHaveBeenCalledWith("/blog/1", expect.any(Object));
  });
});
