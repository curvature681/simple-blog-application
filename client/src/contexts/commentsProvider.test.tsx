import { render, waitFor } from "@testing-library/react";
import CommentApi from "@/api/comment";
import { CommentsProvider, CommentsContext } from "./commentsProvider";
import { useContext, useEffect } from "react";

jest.mock("@/api/comment");

const TestComponent = () => {
  const context = useContext(CommentsContext);
  useEffect(() => {
    if (context) {
      context.fetchCommentForPost(1);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context?.fetchCommentForPost]);

  if (!context) {
    return null;
  }

  const { comments, isLoading } = context;
  return (
    <div>
      <div data-testid="isLoading">{String(isLoading)}</div>
      <div data-testid="comments">{JSON.stringify(comments)}</div>
    </div>
  );
};

describe("CommentsProvider", () => {
  it("provides the correct initial state", async () => {
    const apiRespnse: ApiComment[] = [
      {
        id: 1,
        postId: 1,
        content: "Comment 1",
        author: null,
      },
    ];
    CommentApi.getCommentsByPost = jest.fn().mockResolvedValue(apiRespnse);

    const { getByTestId } = render(
      <CommentsProvider>
        <TestComponent />
      </CommentsProvider>
    );

    expect(getByTestId("isLoading").textContent).toBe("true");

    await waitFor(() =>
      expect(getByTestId("isLoading").textContent).toBe("false")
    );

    expect(getByTestId("comments").textContent).toBe(
      JSON.stringify(apiRespnse)
    );
    expect(CommentApi.getCommentsByPost).toHaveBeenCalledTimes(1);
  });
});
