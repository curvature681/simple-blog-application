import { FastifyReply, FastifyRequest, FastifySchema } from 'fastify';
import { CommentType, NewComment, RequestIdParamType, Comment, NewCommentType } from './comment.types';
import { CommentService } from '@/service/comment.service';

const commentService = new CommentService();

export default class AddCommentAction {
  static readonly PATH = '/post/:id/comment';

  public static action = async (
    request: FastifyRequest<{ Body: NewCommentType; Params: RequestIdParamType; Reply: CommentType }>,
    reply: FastifyReply
  ) => {
    const { id } = request.params;
    const comment = await commentService.addComment(id, request.body);
    await reply.send(comment);
  };

  public static schema(): FastifySchema {
    return {
      tags: ['Comment'],
      body: NewComment,
      params: {
        type: 'object',
        properties: {
          id: { type: 'number' }
        }
      },
      response: {
        200: {
          ...Comment,
          example: {
            id: 1123,
            postId: 1,
            content: 'My comment',
            author: 'john132'
          }
        }
      }
    };
  }
}
