"use client";
import useCommentsContext from "@hooks/useCommentsContext";
import { Box } from "@ui";

export type CommentDetailProps = {
  readonly commentId: number;
};

export default function CommentDetail({ commentId }: CommentDetailProps) {
  const { comments } = useCommentsContext();

  const comment = comments.find((comment) => comment.id === commentId);

  if (!comment) {
    return;
  }

  return (
    <Box
      key={comment.id}
      title={comment.author ?? undefined}
      content={comment.content}
    />
  );
}
