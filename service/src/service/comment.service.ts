import { NewCommentType } from '@/controller/comment/comment.types';
import { NotFound } from '@/error';
import PostRepository from '@/repository/post.repository';
import CommentRepository, { Comment } from '@/repository/comment.repository';

export class CommentService {
  private postRepository: PostRepository;
  private commentRepository: CommentRepository;

  constructor() {
    this.postRepository = new PostRepository();
    this.commentRepository = new CommentRepository();
  }

  async addComment(postId: number, comment: NewCommentType): Promise<Comment> {
    const post = await this.postRepository.findById(postId);
    if (!post) {
      throw new NotFound('Comment not found');
    }

    // We don't insert into DB, just return the comment

    return {
      ...comment,
      createdAt: new Date(),
      author: comment.author || null,
      postId: postId,
      id: Math.floor(10 + Math.random() * 1000)
    };
  }

  async updateComment(id: number, toUpdate: NewCommentType): Promise<Comment> {
    const comment = await this.commentRepository.findById(id);
    if (!comment) {
      throw new NotFound('Post not found');
    }

    // We don't update into DB, just return the comment

    return {
      ...comment,
      content: toUpdate.content,
      author: toUpdate.author || comment.author
    };
  }

  async getByPostId(postId: number): Promise<Comment[]> {
    const post = await this.postRepository.findById(postId);
    if (!post) {
      throw new NotFound('Post not found');
    }

    return await this.commentRepository.findByPostId(postId);
  }
}
