import type { Meta, StoryObj } from "@storybook/react";

import Page from "./page";
import Wrapper from "@/components/Wrapper";

const meta = {
  title: "Pages/Home",
  component: Page,
  parameters: {
    mockData: [
      {
        url: "http://localhost:8080/api/post",
        method: "GET",
        status: 200,
        response: [
          {
            id: 1,
            title: "Hello World",
            content: "This is a test post",
            comments: 0,
          },
          {
            id: 2,
            title: "Hello World 2",
            content: "This is a test post 2",
            comments: 3232,
          },
        ] as ApiPost[],
      },
    ],
  },
  decorators: [
    (Story) => (
      <Wrapper>
        <Story />
      </Wrapper>
    ),
  ],

  tags: ["autodocs"],
} satisfies Meta<typeof Page>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
